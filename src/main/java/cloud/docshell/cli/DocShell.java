package cloud.docshell.cli;

import com.beust.jcommander.JCommander;

import cloud.docshell.cli.parser.AppendCommand;
import cloud.docshell.cli.parser.CommonConfig;
import cloud.docshell.cli.parser.CreateCommand;
import cloud.docshell.cli.parser.ExportCommand;

public class DocShell {

	public static void main(final String[] args) {
		final CreateCommand createCommand = new CreateCommand();
		final AppendCommand appendCommand = new AppendCommand();
		final ExportCommand exportCommand = new ExportCommand();
		final CommonConfig config2 = new CommonConfig();
		final JCommander jcommander = JCommander.newBuilder()
				.addObject(config2)
				.addCommand(appendCommand)
				.addCommand(createCommand)
				.addCommand(exportCommand)
				.build();
		jcommander.parse(args);

		if (config2.isPrintVersion()) {
			// TODO
			return;
		}

		if (config2.isPrintHelp()) {
			printHelp();
			return;
		}

		final String command = jcommander.getParsedCommand();
		if (command == null) {
			printHelp();
			return;
		}
		try {
			switch (command) {
				case CreateCommand.NAME:
					createCommand.exec();
					break;
				case AppendCommand.NAME:
					appendCommand.exec();
					break;
				case ExportCommand.NAME:
					break;
				default:
					break;
			}
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static void printHelp() {
		System.out.println("docshell create docshell [-o|--output-file <<output-file>>] [[-s|--script-file <<script-file>>] ...] [-t|--title <<title>>] [-a|--author <<author>>] [-d|--date <<date>>] [--shell-type [BASH][POSTGRES][MYSQL]]");

	}
}
