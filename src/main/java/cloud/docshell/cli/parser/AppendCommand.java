package cloud.docshell.cli.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.schema.docshell.Docshell;

@Parameters(commandNames = AppendCommand.NAME, commandDescription = "append data to existing file")
public class AppendCommand {

	public final static String NAME = "append";

	@Parameter(names = "server")
	private boolean server;

	@Parameter(names = "docshell")
	private boolean docshell;

	// docshell and server
	@Parameter(names = { "-o", "--output-file" }, required = true)
	private File outputFile;

	// docshell only
	@Parameter(names = { "-s", "--script-file" }, variableArity = true)
	private List<File> scriptFiles;

	@Parameter(names = { "--shell-type" }, variableArity = true)
	private List<String> shellTypes;

	@Parameter(names = { "-c", "--config-file" })
	private File configFile;

	// server only
	// TODO
	private void validate() {
		if (this.server && this.docshell) {
			throw new ParameterException("Specify just one between server and docshell");
		}

		// TODO
	}

	public void exec() throws IOException, LineParseException, JAXBException, ParserConfigurationException, TransformerException {
		this.validate();
		if (this.docshell) {
			this.docshell();
			return;
		}
		// TODO
		throw new UnsupportedOperationException("not yet implemented");
	}

	private void docshell() throws IOException, LineParseException, JAXBException, ParserConfigurationException, TransformerException {
		Docshell ds;
		final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
		try (InputStream is = new FileInputStream(this.outputFile)) {
			final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			ds = (Docshell) unmarshaller.unmarshal(is);
		}

		Util.toDocshell(ds, this.shellTypes, this.scriptFiles, this.outputFile);
	}
}
