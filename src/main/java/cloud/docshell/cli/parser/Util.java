package cloud.docshell.cli.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import cloud.docshell.cli.importer.CommandParser;
import cloud.docshell.cli.importer.command.CommandFactory;
import cloud.docshell.cli.importer.command.CommandSelector;
import cloud.docshell.cli.importer.line.LineParseException;
import cloud.docshell.cli.importer.line.XTermParser;
import cloud.docshell.schema.docshell.Docshell;

public class Util {

	private static final String STYLE = "https://www.docshell.cloud/xsl/docshell.xsl";

	public static void toDocshell(final Docshell ds,
			final List<String> shellTypes,
			final List<File> scriptFiles,
			final File outputFile)
			throws LineParseException, IOException, FileNotFoundException, JAXBException, PropertyException, ParserConfigurationException, TransformerException {
		final CommandParser commandParser = new CommandParser(ds);
		final XTermParser xTermParser = new XTermParser();

		final CommandSelector[] selectors;
		if (shellTypes == null || shellTypes.isEmpty()) {
			selectors = new CommandSelector[] { CommandFactory.BASH };
		} else {
			selectors = shellTypes.stream().map(CommandFactory::getCommandSelector).filter(f -> f != null).toArray(l -> new CommandSelector[l]);
		}

		if (scriptFiles == null) {
			commandParser.parse(xTermParser, System.in, selectors);
		} else {
			for (final File scriptFile : scriptFiles) {
				try (FileInputStream fis = new FileInputStream(scriptFile)) {
					commandParser.parse(xTermParser, fis, selectors);
				}
			}
		}
		{

			final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			final DocumentBuilder db = dbf.newDocumentBuilder();
			final Document document = db.newDocument();
			final JAXBContext jaxbContext = JAXBContext.newInstance(Docshell.class);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(ds, document);
			addXsl(document);
			final TransformerFactory tf = TransformerFactory.newInstance();
			final Transformer t = tf.newTransformer();
			// copy from here https://stackoverflow.com/a/1384816
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			final DOMSource source = new DOMSource(document);
			try (OutputStream os = outputFile == null ? System.out : new FileOutputStream(outputFile)) {
				final StreamResult result = new StreamResult(os);
				t.transform(source, result);
			}
		}
	}

	private static void addXsl(final Document doc) {
		final Node pi = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + STYLE + "\"");
		final Element root = doc.getDocumentElement();
		doc.insertBefore(pi, root);
	}
}
