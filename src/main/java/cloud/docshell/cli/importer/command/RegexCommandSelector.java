package cloud.docshell.cli.importer.command;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cloud.docshell.schema.docshell.Command;
import cloud.docshell.schema.docshell.Io;
import cloud.docshell.schema.docshell.IoType;
import cloud.docshell.schema.docshell.KeyValue;

public class RegexCommandSelector implements CommandSelector {

	private final String shellType;
	private final String regex;
	private final Integer inputGroup;
	private final String[] paramNames;

	public RegexCommandSelector(final String shellType, final String regex, final Integer inputGroup, final String... paramNames) {
		super();
		this.shellType = shellType;
		this.regex = regex;
		this.inputGroup = inputGroup;
		this.paramNames = paramNames;
	}

	@Override
	public String getShellType() {
		return this.shellType;
	}

	@Override
	public Command getCommand(final String line) {
		final Pattern pattern = Pattern.compile(this.regex);
		final Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			final Command command = new Command();
			command.setShellType(this.shellType);
			command.setId(this.getShellType() + "-" + UUID.randomUUID());
			if (this.inputGroup != null) {
				final Io io = new Io();
				io.setValue(matcher.group(this.inputGroup.intValue()));
				io.setType(IoType.INPUT);
				command.getIo().add(io);
			}

			for (int i = 0; i < this.paramNames.length; i++) {
				final String paramName = this.paramNames[i];
				if (paramName != null) {
					final KeyValue property = new KeyValue();
					property.setKey(paramName);
					property.setValue(matcher.group(i));
					command.getProperty().add(property);
				}
			}

			return command;
		}
		return null;
	}

}
