package cloud.docshell.cli.importer.command;

import java.util.HashMap;

public class CommandFactory {

	public static final CommandSelector BASH = new RegexCommandSelector("BASH", "^(.+)@(.+):(.+)[\\$#] (.+)", Integer.valueOf(4), null, "user", "serverName", "wd");
	public static final CommandSelector POSTGRESQL = new RegexCommandSelector("POSTGRESQL", "^(.+)([\\(=-][>#])(.*)", Integer.valueOf(3), null, "database", "prompt");
	public static final CommandSelector MYSQL = new RegexCommandSelector("MYSQL", "^mysql>(.*)", Integer.valueOf(1));
	public static final CommandSelector GPG = new RegexCommandSelector("GPG", "^gpg> (.*)", Integer.valueOf(1));

	private final static HashMap<String, CommandSelector> COMMAND_SELECTOR_MAP = new HashMap<>();

	static {
		COMMAND_SELECTOR_MAP.put(BASH.getShellType(), BASH);
		COMMAND_SELECTOR_MAP.put(POSTGRESQL.getShellType(), POSTGRESQL);
		COMMAND_SELECTOR_MAP.put(MYSQL.getShellType(), MYSQL);
		COMMAND_SELECTOR_MAP.put(GPG.getShellType(), GPG);
	}

	public static CommandSelector getCommandSelector(final String shellType) {
		return COMMAND_SELECTOR_MAP.get(shellType);
	}
}
