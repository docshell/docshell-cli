package cloud.docshell.cli.importer.command;

import cloud.docshell.schema.docshell.Command;

public interface CommandSelector {

	String getShellType();

	Command getCommand(String line);
}
