package cloud.docshell.cli.importer.line;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

//https://www.x.org/docs/xterm/ctlseqs.pdf
public class XTermParser implements LineParser {

	private final static char ESC = 27;
	private final static char BELL = 7;
	private final static char BACKSPACE = 8;

	@Override
	public List<String> lines(final InputStream is) throws LineParseException {
		try {
			final List<String> lines = CrLfParser
					.visibleLine(new InputStreamReader(is))
					.stream()
					.map(this::line)
					.collect(Collectors.toList());
			return lines;
		} catch (final IOException ex) {
			throw new LineParseException(ex);
		}
	}

	public String line(final String line) {
		String result = removeSaveRestoreCursor(line);
		result = removeChangeTitle(result);
		result = removeFormat(result);
		result = removeEraseInLine(result);
		result = removeGSequence(result);
		result = removeSequence(result, new char[] { ESC, '>' });
		result = removeBell(result);
		return result;
	}

	private static String removeSaveRestoreCursor(final String line) {

		final char[] seq1 = { ESC, '7' }; // save cursor
		final char[] seq2 = { ESC, '8' }; // restore cursor

		int state = 0;
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (state >= seq1.length) {
				if (state >= seq2.length + seq1.length) {
					state = 0;
				} else if (seq2[state - seq1.length] == c) {
					state++;
				} else if (state > seq1.length) {
					state = seq1.length;
				}
			}
			if (state < seq1.length) {
				if (seq1[state] == c) {
					state++;
				} else {
					for (int j = 0; j < state; j++) {
						result.append(seq1[j]);
					}
					result.append(c);
					state = 0;
				}
			}
		}
		return result.toString();
	}

	private static String removeFormat(final String line) {
		int stato = 0;
		final byte[] seq = { 27, 91 };
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (stato >= seq.length) {
				if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z') {
					stato = 0;
				}
			} else if (c == seq[stato]) {
				stato++;
			} else {
				for (int j = 0; j < stato; j++) {
					result.append((char) seq[j]);
				}
				stato = 0;
				result.append(c);
			}
		}
		final String string = result.toString();
		return string;
	}

	private static String removeGSequence(final String line) {
		String result = removeGSequence(line, new char[] { ESC, '(' });
		result = removeGSequence(result, new char[] { ESC, ')' });
		result = removeGSequence(result, new char[] { ESC, '*' });
		result = removeGSequence(result, new char[] { ESC, '+' });
		return result;
	}

	private static String removeGSequence(final String line, final char[] seq) {
		int stato = 0;
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (stato >= seq.length) {
				if (c == '0' || c == 'A' || c == 'B') {
					stato = 0;
				}
			} else if (c == seq[stato]) {
				stato++;
			} else {
				for (int j = 0; j < stato; j++) {
					result.append(seq[j]);
				}
				stato = 0;
				result.append(c);
			}
		}
		final String string = result.toString();
		return string;
	}

	private static String removeChangeTitle(final String line) {
		int stato = 0;
		final byte[] seq = { ESC, ']' };
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (stato >= seq.length) {
				if (c == BELL) {
					stato = 0;
				}
			} else if (c == seq[stato]) {
				stato++;
			} else {
				for (int j = 0; j < stato; j++) {
					result.append((char) seq[j]);
				}
				stato = 0;
				result.append(c);
			}
		}
		final String string = result.toString();
		return string;
	}

	public static String removeEraseInLine(final String line) {
		int stato = 0;
		final byte[] seq = { BACKSPACE };
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (c == seq[stato]) {
				stato++;
				if (stato == seq.length) {
					result.deleteCharAt(result.length() - 1);
					stato = 0;
				}
			} else {
				for (int j = 0; j < stato; j++) {
					result.append((char) seq[j]);
				}
				stato = 0;
				result.append(c);
			}
		}
		final String string = result.toString();
		return string;
	}

	public static String removeSequence(final String line, final char[] seq) {
		int stato = 0;
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (c == seq[stato]) {
				stato++;
				if (stato == seq.length) {
					stato = 0;
				}
			} else {
				for (int j = 0; j < stato; j++) {
					result.append(seq[j]);
				}
				stato = 0;
				result.append(c);
			}
		}
		final String string = result.toString();
		return string;
	}

	private static String removeBell(final String line) {
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (c != BELL) {
				result.append(c);
			}
		}
		return result.toString();
	}
}
