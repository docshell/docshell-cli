package cloud.docshell.cli.importer.line;

import java.io.InputStream;
import java.util.List;

public interface LineParser {

	List<String> lines(InputStream is) throws LineParseException;

}
